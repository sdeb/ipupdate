# ipupdate changelog

tags utilisés: added  changed  cosmetic  deprecated  fixed  new rewriting  removed  syncro  security
the date in format: YYYY-MM-DD

## [Unreleased]



## [1.7.0] - 19.07.2019

* rewriting: f__basedirname, f__color
* rewriting: localisation .conf, .logs
* changed: script_install_special, les droits sont préservés pour .conf, les logs dans le répertoire courant ne sont pas déplacés

## [1.6.0] - 2018.06.28

* syncro: f__basedirname
* synchro: f_n_dig, f_n_host

## [1.5.0] - 2018.06.22

* fixed: correction en place/installé

* new : f__sudo
* rewriting: functions script_* 
* rewriting: sudo lors traitement option, f_affichage
* fixed: uninstall, appel script_uninstall_special

## [1.4.1] - 2018.06.20

* rewriting: script_get_version, script_install, script_uninstall, script_upgrade, script_install_special, script_uninstall_special
* cosmetic: script_get_version

## [1.3.0] - 2018.06.18
* syncro: f__random
* cosmetic: f_ip_dns, f_n_host, f_n_dig
* changed: syncro & renommage f__cnx -> f_n_cnx
* rewriting: f_ip_validate, syncro
* rewriting: f_ip_dns, prise en charge f_n_host f_n_dig
* rewriting: f_ip_master, prise en charge f_n_host f_n_dig
* rewriting: f_ip_pub, prise en charge f_n_host f_n_dig
* added: f_n_host f_n_dig
* added: compatibily for ArchLinux, drill instead dig, f_n_dig, f_ip_dns, f_ip_master, f_ip_pub
* added: version en place

## [1.2.0] - 2018.06.16

* syncro: f__cnx
* fixed: f__cnx, f_ip_pub
* fixed: ip publiques
* added: ip pub dans infos help

## [1.1.1] - 2018.06.05
* cosmetic: various, renaming functions, better homogeneity with other scripts
* changed: makerun 1.1
* new: makefile 1.4

## [1.1.0] - 2018.6.03
* added: ipv4/ipv6 detection
* added: option --dev

## [1.0.2] - 2018.06.02
* fixed: display of online version

## [1.0.1] - 2018.05.31
* fixed: display of online version

## [1.0.0] - 2018.05.31
* initial commit, first public release 
